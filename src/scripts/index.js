import '../styles/index.scss';
import * as moment from 'moment';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';

console.log('webpack starterkit');
console.log(moment().format('LLL'));

let params = {
    api_key: 'znWsq6ew0QIoK1eJKG9AMIM9fhFHyHZm',
    q: 'Matrix' 
};

var $container;
var apiUrl;

$(document).ready(function() {

    $container = $('#gifs');
    $('#searchbutton').click(function() {
        $container.empty();
        params.q = $('#inputsearch').val();
        apiUrl = `https://cors-anywhere.herokuapp.com/api.giphy.com/v1/gifs/search?${$.param(params)}`;
        console.log(params.q);
        $.ajax({
            url: apiUrl,
            type: 'GET',

        }).done(data => {
            console.log(data);
            
            renderGifs(data);
            
        });
    });
    
    
});

function renderGifs(gifs) {
    $.each(gifs.data, (i, gif) => {
        $container.append(display(gif.images.original.url));
    });
}


function display(photo) {
    return `
    <div class="col-sm-12 col-md-4">
        <div class="card">
            <div class="card-img">
                <img src="${photo}" alt="" class="card-img-top">
            </div>
        </div>
    </div>
    `;
};

